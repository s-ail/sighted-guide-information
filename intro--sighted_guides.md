Here are some notes on the way to a sighted guide training curriculum

## proposed takeaways for participants

- note that most of sailing requires no vision, and that different vision imparements have different needs (image [for illustration](http://eurotechoptical.com/images/virtuemart/product/VisualEyes%20Vision%20Simulator%20Glasses%20200773.jpg))
- ways to understand what useful information is missing, and ways to convey it
- ways to stay out of the way, including staying out of most decisions

## some how-tos to try out and some concepts to have in mind

empowering individual sailors (vs ferrying passengers); guiding (vs instructing)

- asking what info an individual sailor needs for situational awareness, then conveying it
- enabling decisions to sail (describing conditions in a way that sailors can discuss characteristics of boat and group's skills)

for new sailors, treating rigging & hoisting like an escape room challenge (guide staying out of the way)

- for b1 (shall we use these BSI designations?), describing anything the boom might hit if it swings

practice communicating around leaving the dock

- describing factors needed for sailors to create a plan and assign roles
- agreeing how the guide will communicate (e.g. terms used for steering and trimming)

practice communicating in typical open-water sailing scenarios

- agree on terms for apparent wind direction (esp when points of sail may not be familiar; flag guiding vs instructing)
- heading up & bearing away
- maintaining a close hauled course
- prepping for / executing a tack / gybe

practice communicating in typical open-water sailing scenarios

- communicating about a rounding

landing at the dock
